import {Game} from "./models/Game";
import {Point} from "./models/Point";

const game = new Game();

while (true) {
    // @ts-ignore
    var inputs: string[] = readline().split(' ');
    const opponentRow: number = parseInt(inputs[0], 10);
    const opponentCol: number = parseInt(inputs[1], 10);

    game.addEnemysPoints(
        new Point({
            x: opponentRow,
            y:opponentCol
        })
    );

    // @ts-ignore
    const validActionCount: number = parseInt(readline(), 10);
    game.initAvailablePoint();

    for (let i = 0; i < validActionCount; i++) {
        // @ts-ignore
        var inputs: string[] = readline().split(' ');
        const row: number = parseInt(inputs[0], 10);
        const col: number = parseInt(inputs[1], 10);
        game.addAvailablePoint(
            new Point({
                x: row,
                y:col
            })
        );
    }
    console.log(game.nextAction());
}

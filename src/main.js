(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./Entity"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Entity_1 = require("./Entity");
    // @ts-ignore
    var inputs = readline().split(' ');
    var baseX = parseInt(inputs[0]); // The corner of the map representing your base
    var baseY = parseInt(inputs[1]);
    var height = 9000;
    var width = 17630;
    var diagonal = Math.pow(Math.pow(height, 2) + Math.pow(width, 2), 0.5);
    var heroRayon = 1280;
    // @ts-ignore
    var heroesPerPlayer = parseInt(readline()); // Always 3
    var initDirections = function (hero) {
        if (hero.id === 0) {
            return "2200 4200";
        }
        else if (hero.id === 1) {
            return "3800 4400";
        }
        else if (hero.id === 2) {
            return "4600 1500";
        }
        else if (hero.id === 3) {
            return "16000 4900";
        }
        else if (hero.id === 4) {
            return "14000 6000";
        }
        return "13300 7500";
    };
    var distanceFromTwoEntities = function (entity1, entity2) {
        var a = entity1.coords.x - entity2.coords.x;
        var b = entity1.coords.y - entity2.coords.y;
        var c = Math.pow(a, 2) + Math.pow(b, 2);
        return Math.pow(c, 0.5);
    };
    var allMonsters = function (entities) {
        return entities.filter(function (entitie) {
            return entitie.isMonster();
        });
    };
    var findById = function (entities, id) {
        return entities.filter(function (entity) {
            if (entity.id === id) {
                return true;
            }
        })[0];
    };
    var sortMonsters = function (monsters) {
        return monsters.sort(function (a, b) {
            return b.threat - a.threat;
        });
    };
    var getMonsterToAttack = function (monsters) {
        for (var i = 0; i < monsters.length; i++) {
            var monster = monsters[i];
            if (monster.attackedBy.length === 0) {
                return monster;
            }
        }
        return monsters[0];
    };
    var formatAttack = function (monster) {
        return "".concat(monster.coords.x, " ").concat(monster.coords.y);
    };
    var prepareAttack = function (monster, hero) {
        monster.attackedBy.push(hero.id);
    };
    var enoughMana = function (mana) {
        return mana >= 10;
    };
    var playWithTheReds = function (heros) {
        return heros[0].id === 3;
    };
    while (true) {
        var mana = void 0;
        for (var i = 0; i < 2; i++) {
            // @ts-ignore
            var inputs = readline().split(' ');
            var health = parseInt(inputs[0]); // Each player's base health
            mana = parseInt(inputs[1]); // Ignore in the first league; Spend ten mana to cast a spell
        }
        // @ts-ignore
        var entityCount = parseInt(readline()); // Amount of heros and monsters you can see
        var entities = [];
        for (var i = 0; i < entityCount; i++) {
            // @ts-ignore
            var inputs = readline().split(' ');
            var id = parseInt(inputs[0]); // Unique identifier
            var type = parseInt(inputs[1]); // 0=monster, 1=your hero, 2=opponent hero
            var x = parseInt(inputs[2]); // Position of this entity
            var y = parseInt(inputs[3]);
            var shieldLife = parseInt(inputs[4]); // Ignore for this league; Count down until shield spell fades
            var isControlled = parseInt(inputs[5]); // Ignore for this league; Equals 1 when this entity is under a control spell
            var health = parseInt(inputs[6]); // Remaining health of this monster
            var vx = parseInt(inputs[7]); // Trajectory of this monster
            var vy = parseInt(inputs[8]);
            var nearBase = parseInt(inputs[9]); // 0=monster with no target yet, 1=monster targeting a base
            var threatFor = parseInt(inputs[10]); // Given this monster's trajectory, is it a threat to 1=your base, 2=your opponent's base, 0=neither
            var entity = new Entity_1.Entity({
                id: id,
                type: type,
                x: x,
                y: y,
                shieldLife: shieldLife,
                isControlled: isControlled,
                health: health,
                vx: vx,
                vy: vy,
                nearBase: nearBase,
                threatFor: threatFor,
                diagonal: diagonal,
                baseX: baseX,
                baseY: baseY
            });
            entities.push(entity);
        }
        for (var i = 0; i < heroesPerPlayer; i++) {
            var indexHero = i;
            if (playWithTheReds(entities)) {
                indexHero += 3;
            }
            var hero = findById(entities, indexHero);
            var monsters = allMonsters(entities);
            var go = void 0;
            if (monsters.length) {
                monsters = sortMonsters(monsters);
                var monster = getMonsterToAttack(monsters);
                prepareAttack(monster, hero);
                go = "MOVE " + formatAttack(monster);
            }
            if (!go) {
                go = "MOVE " + initDirections(findById(entities, indexHero));
            }
            console.log(go);
        }
    }
});

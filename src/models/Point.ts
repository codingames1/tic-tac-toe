export class Point {
    public readonly x: number;
    public readonly y: number;

    constructor({ x, y }) {
        this.x = x;
        this.y = y;
    }

    public format = (): string => `${this.x} ${this.y}`;

    public isAvailable(availablePoints: Point[]): boolean {
        for (const availablePoint of availablePoints) {
            if (availablePoint.x === this.x && availablePoint.y === this.y) {
                return true;
            }
        }
        return false;
    }
}

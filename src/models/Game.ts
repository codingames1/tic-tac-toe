import { Point } from "./Point";

export class Game {
    private availablePoints: Point[] = [];
    private myPoints: Point[] = [];
    private enemysPoints: Point[] = [];
    static closeToWin = 2;

    private getSolutions(): Solution[] {
        return [
            {
                solutionPoints: [new Point({ x: 0, y: 0 }), new Point({ x: 0, y: 1 }), new Point({ x: 0, y: 2 })],
                index: 1,
            },
            {
                solutionPoints: [new Point({ x: 0, y: 0 }), new Point({ x: 0, y: 1 }), new Point({ x: 0, y: 2 })],
                index: 2,
            },
            {
                solutionPoints: [new Point({ x: 0, y: 0 }), new Point({ x: 0, y: 1 }), new Point({ x: 0, y: 2 })],
                index: 3,
            },
            {
                solutionPoints: [new Point({ x: 1, y: 0 }), new Point({ x: 1, y: 1 }), new Point({ x: 1, y: 2 })],
                index: 4,
            },
            {
                solutionPoints: [new Point({ x: 2, y: 0 }), new Point({ x: 2, y: 1 }), new Point({ x: 2, y: 2 })],
                index: 5,
            },
            {
                solutionPoints: [new Point({ x: 0, y: 0 }), new Point({ x: 1, y: 0 }), new Point({ x: 2, y: 0 })],
                index: 6,
            },
            {
                solutionPoints: [new Point({ x: 0, y: 1 }), new Point({ x: 1, y: 1 }), new Point({ x: 2, y: 1 })],
                index: 7,
            },
            {
                solutionPoints: [new Point({ x: 0, y: 2 }), new Point({ x: 1, y: 2 }), new Point({ x: 2, y: 2 })],
                index: 8,
            },
            {
                solutionPoints: [new Point({ x: 0, y: 0 }), new Point({ x: 1, y: 1 }), new Point({ x: 2, y: 2 })],
                index: 9,
            },
            {
                solutionPoints: [new Point({ x: 0, y: 2 }), new Point({ x: 1, y: 1 }), new Point({ x: 2, y: 0 })],
                index: 10,
            },
        ];
    }

    public initAvailablePoint() {
        this.availablePoints = [];
    }

    public addAvailablePoint(pointToAdd: Point) {
        this.availablePoints.push(pointToAdd);
    }

    public addMyPoints(pointToAdd: Point) {
        this.myPoints.push(pointToAdd);
    }

    public addEnemysPoints(pointToAdd: Point) {
        this.enemysPoints.push(pointToAdd);
    }

    public getPointToCheck(): Point {
        const iCanWin: CanWin = this.closeToWin(this.myPoints);
        const enemyCanWin: CanWin = this.closeToWin(this.enemysPoints);

        if (iCanWin.canWin) {
            return this.findWinnerPoint(this.getSolutionByIndex(iCanWin.solution).solutionPoints, this.myPoints);
        } else if (enemyCanWin.canWin) {
            return this.findWinnerPoint(
                this.getSolutionByIndex(enemyCanWin.solution).solutionPoints,
                this.enemysPoints
            );
        }
        return this.getRandomAvailablePoint();
    }

    private getRandomAvailablePoint(): Point {
        return this.availablePoints[0];
    }

    private getSolutionByIndex(indexToSearch: number): Solution {
        for (const solution of this.getSolutions()) {
            if (solution.index === indexToSearch) {
                return solution;
            }
        }
    }

    private closeToWin(points: Point[]): CanWin {
        for (const { solutionPoints, index } of this.getSolutions()) {
            if (this.countCommonPoint(solutionPoints, points) === Game.closeToWin) {
                const pointToWin = this.findWinnerPoint(solutionPoints, this.myPoints);
                if (pointToWin.isAvailable(this.availablePoints)) {
                    return {
                        canWin: true,
                        solution: index,
                    };
                }
            }
        }
        return {
            canWin: false,
        };
    }

    private countCommonPoint(solution: SolutionPoint, pointsToCheck: Point[]): number {
        let count: number = 0;
        for (const pointSolution of solution) {
            for (const point of pointsToCheck) {
                if (point.x === pointSolution.x && point.y === pointSolution.y) {
                    count++;
                }
            }
        }
        return count;
    }

    private findWinnerPoint(solution, points): Point {
        for (const pointSolution of solution) {
            let isGoodSolution = true;
            for (const pointToCheck of points) {
                if (pointToCheck.x === pointSolution.x && pointToCheck.y === pointSolution.y) {
                    isGoodSolution = false;
                }
            }
            if (isGoodSolution) {
                return pointSolution;
            }
        }
    }

    public nextAction(): string {
        const point: Point = this.getPointToCheck();
        this.addMyPoints(point);
        return point.format();
    }
}

type CanWin = {
    canWin: boolean;
    solution?: number;
};

type SolutionPoint = Array<Point>;

type Solution = {
    solutionPoints: SolutionPoint;
    index: number;
};
